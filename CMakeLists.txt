cmake_minimum_required(VERSION 3.5)

project(lomiri-app-launch VERSION 0.1.9 LANGUAGES C CXX)

##########################
# Version Info
##########################

set(SOVERSION_MAJOR 0)
set(SOVERSION_MINOR 0)
set(SOVERSION_PATCH 0)

set(ABI_VERSION ${SOVERSION_MAJOR})

##########################
# Options
##########################

option (ENABLE_TESTS "Build tests" ON)
option (ENABLE_COVERAGE "Enable coverage reports" ON)
option (USE_SYSTEMD "Use systemd jobs manager instead of POSIX" ON)
option (ENABLE_MIRCLIENT "Enable helper session using mirclient" ON)

set(AA_EXEC_PATH "/usr/bin/aa-exec" CACHE STRING "Path to aa-exec (AppArmor)")

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake" "${CMAKE_MODULE_PATH}")

set(PACKAGE ${CMAKE_PROJECT_NAME})

##########################
# Dependent Packages
##########################

find_package(PkgConfig REQUIRED)

if (USE_SYSTEMD)
	pkg_check_modules(SYSTEMD systemd)
	if (NOT SYSTEMD_FOUND)
		message(FATAL_ERROR "SYSTEMD has not been found!")
	endif ()
endif ()

find_package(GObjectIntrospection REQUIRED)
include(GNUInstallDirs)
include(CheckIncludeFile)
include(CheckFunctionExists)
include(UseGlibGeneration)
include(UseGdbusCodegen)
include(UseConstantBuilder)
include(UseLttngGenTp)

# Workaround for libexecdir on debian
if (EXISTS "/etc/debian_version") 
  set(CMAKE_INSTALL_LIBEXECDIR ${CMAKE_INSTALL_LIBDIR})
  set(CMAKE_INSTALL_FULL_LIBEXECDIR "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBEXECDIR}")
endif()

set(pkglibexecdir "${CMAKE_INSTALL_FULL_LIBEXECDIR}/${CMAKE_PROJECT_NAME}")
set(CMAKE_INSTALL_PKGLIBEXECDIR "${CMAKE_INSTALL_LIBEXECDIR}/${CMAKE_PROJECT_NAME}")
set(CMAKE_INSTALL_FULL_PKGLIBEXECDIR "${CMAKE_INSTALL_FULL_LIBEXECDIR}/${CMAKE_PROJECT_NAME}")
set(CMAKE_INSTALL_FULL_PKGDATADIR "${CMAKE_INSTALL_FULL_DATADIR}/${CMAKE_PROJECT_NAME}")

if(NOT LOMIRI_APP_LAUNCH_ARCH)
	execute_process(COMMAND dpkg-architecture -qDEB_BUILD_MULTIARCH
		OUTPUT_VARIABLE LOMIRI_APP_LAUNCH_ARCH
		OUTPUT_STRIP_TRAILING_WHITESPACE
	)
	if(NOT LOMIRI_APP_LAUNCH_ARCH)
		message(FATAL_ERROR "LOMIRI_APP_LAUNCH_ARCH is empty!")
	endif()
endif()

add_compile_options(
# Because we believe in quality
	-Wall -Werror
	-g
	-pthread
)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=gnu99")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17")

pkg_check_modules(GLIB2 REQUIRED glib-2.0)
include_directories(${GLIB2_INCLUDE_DIRS})

pkg_check_modules(GOBJECT2 REQUIRED gobject-2.0)
include_directories(${GOBJECT2_INCLUDE_DIRS})

pkg_check_modules(GIO2 REQUIRED gio-2.0 gio-unix-2.0)
include_directories(${GIO2_INCLUDE_DIRS})

pkg_check_modules(JSONGLIB REQUIRED json-glib-1.0>=1.1.2)
include_directories(${JSONGLIB_INCLUDE_DIRS})

pkg_check_modules(ZEITGEIST REQUIRED zeitgeist-2.0)
include_directories(${ZEITGEIST_INCLUDE_DIRS})

pkg_check_modules(CLICK REQUIRED click-0.4>=0.4.18)
include_directories(${CLICK_INCLUDE_DIRS})

pkg_check_modules(DBUS REQUIRED dbus-1)
include_directories(${DBUS_INCLUDE_DIRS})

if(${ENABLE_TESTS})
  pkg_check_modules(DBUSTEST REQUIRED dbustest-1>=14.04.0)
  include_directories(${DBUSTEST_INCLUDE_DIRS})
endif()

pkg_check_modules(LTTNG REQUIRED lttng-ust)
include_directories(${LTTNG_INCLUDE_DIRS})

if(ENABLE_MIRCLIENT)
  pkg_check_modules(MIR REQUIRED mirclient)
  include_directories(${MIR_INCLUDE_DIRS})
endif()

pkg_check_modules(LIBERTINE libertine)
include_directories(${LIBERTINE_INCLUDE_DIRS})
if(${LIBERTINE_FOUND})
  add_definitions(-DHAVE_LIBERTINE=1)
endif(${LIBERTINE_FOUND})

pkg_check_modules(CURL libcurl>=7.47)
include_directories(${CURL_INCLUDE_DIRS})

pkg_check_modules(LOMIRI_API REQUIRED liblomiri-api>=0.1.0)
include_directories(${LOMIRI_API_INCLUDE_DIRS})

include_directories(${CMAKE_CURRENT_SOURCE_DIR})

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fPIC")

####################
# lomiri-app-launch-desktop.click-hook
####################

configure_file("data/lomiri-app-launch-desktop.click-hook.in" "${CMAKE_CURRENT_SOURCE_DIR}/debian/lomiri-app-launch-desktop.click-hook" @ONLY)

add_subdirectory(data)
add_subdirectory(liblomiri-app-launch)
add_subdirectory(tools)
add_subdirectory(utils)

# testing & coverage
if (ENABLE_TESTS)
  enable_testing ()
  add_subdirectory(tests)
endif ()

if (ENABLE_COVERAGE)
	find_package(CoverageReport)
	set(filter-list)
	list(APPEND filter-list "/usr/include")
	list(APPEND filter-list "${CMAKE_SOURCE_DIR}/tests/*")
	if (NOT ${CMAKE_BINARY_DIR} STREQUAL ${CMAKE_SOURCE_DIR})
		list(APPEND filter-list "${CMAKE_BINARY_DIR}/*")
	endif()
	ENABLE_COVERAGE_REPORT(
		TARGETS
			lomiri-launcher
			launcher-static
			desktop-hook
		TESTS
			app-store-legacy
			application-icon-finder-test
			application-info-desktop-test
			helper-test
			helper-handshake-test
			info-watcher-zg
			jobs-base-test
			jobs-systemd
			liblal-test
			liblal-cpp-test
			list-apps
			snapd-info-test
		FILTER
			${filter-list}
	)
endif ()
